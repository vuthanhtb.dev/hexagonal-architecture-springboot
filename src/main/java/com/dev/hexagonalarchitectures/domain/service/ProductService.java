package com.dev.hexagonalarchitectures.domain.service;

import com.dev.hexagonalarchitectures.application.ports.input.CreateProductUseCase;
import com.dev.hexagonalarchitectures.application.ports.input.GetProductUseCase;
import com.dev.hexagonalarchitectures.application.ports.output.ProductEventPublisher;
import com.dev.hexagonalarchitectures.application.ports.output.ProductOutputPort;
import com.dev.hexagonalarchitectures.domain.event.ProductCreatedEvent;
import com.dev.hexagonalarchitectures.domain.exception.ProductNotFound;
import com.dev.hexagonalarchitectures.domain.model.Product;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ProductService implements CreateProductUseCase, GetProductUseCase {
    private final ProductOutputPort productOutputPort;

    private final ProductEventPublisher productEventPublisher;

    @Override
    public Product createProduct(Product product) {
        product = productOutputPort.saveProduct(product);
        productEventPublisher.publishProductCreatedEvent(new ProductCreatedEvent(product.getId()));
        return product;
    }

    @Override
    public Product getProductById(Long id) {
        return productOutputPort.getProductById(id).orElseThrow(() -> new ProductNotFound("Product not found with id " + id));
    }
}
