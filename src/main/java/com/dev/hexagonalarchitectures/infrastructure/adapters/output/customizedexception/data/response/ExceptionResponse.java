package com.dev.hexagonalarchitectures.infrastructure.adapters.output.customizedexception.data.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse {
    private LocalDateTime date;
    private String message;
    private List<String> details;
}
