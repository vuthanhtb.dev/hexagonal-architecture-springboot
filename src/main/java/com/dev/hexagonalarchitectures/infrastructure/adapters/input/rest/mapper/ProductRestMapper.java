package com.dev.hexagonalarchitectures.infrastructure.adapters.input.rest.mapper;

import com.dev.hexagonalarchitectures.domain.model.Product;
import com.dev.hexagonalarchitectures.infrastructure.adapters.input.rest.data.request.ProductCreateRequest;
import com.dev.hexagonalarchitectures.infrastructure.adapters.input.rest.data.response.ProductCreateResponse;
import com.dev.hexagonalarchitectures.infrastructure.adapters.input.rest.data.response.ProductQueryResponse;
import org.mapstruct.Mapper;

@Mapper
public interface ProductRestMapper {
    Product toProduct(ProductCreateRequest productCreateRequest);

    ProductCreateResponse toProductCreateResponse(Product product);

    ProductQueryResponse toProductQueryResponse(Product product);
}
