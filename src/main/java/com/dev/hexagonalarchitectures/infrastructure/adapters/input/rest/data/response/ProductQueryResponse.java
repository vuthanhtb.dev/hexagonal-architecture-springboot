package com.dev.hexagonalarchitectures.infrastructure.adapters.input.rest.data.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductQueryResponse {
    private Long id;
    private String name;
    private String description;
}
