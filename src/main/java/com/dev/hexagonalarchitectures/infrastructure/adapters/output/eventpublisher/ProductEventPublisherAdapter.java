package com.dev.hexagonalarchitectures.infrastructure.adapters.output.eventpublisher;

import com.dev.hexagonalarchitectures.application.ports.output.ProductEventPublisher;
import com.dev.hexagonalarchitectures.domain.event.ProductCreatedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;

@RequiredArgsConstructor
public class ProductEventPublisherAdapter implements ProductEventPublisher {
    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void publishProductCreatedEvent(ProductCreatedEvent event) {
        applicationEventPublisher.publishEvent(event);
    }
}
