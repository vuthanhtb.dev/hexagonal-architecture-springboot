package com.dev.hexagonalarchitectures.infrastructure.adapters.output.persistence.mapper;

import com.dev.hexagonalarchitectures.domain.model.Product;
import com.dev.hexagonalarchitectures.infrastructure.adapters.output.persistence.entity.ProductEntity;
import org.mapstruct.Mapper;

@Mapper
public interface ProductPersistenceMapper {
    ProductEntity toProductEntity(Product product);
    Product toProduct(ProductEntity productEntity);
}
