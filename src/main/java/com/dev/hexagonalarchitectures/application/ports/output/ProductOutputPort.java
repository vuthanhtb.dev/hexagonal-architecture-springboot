package com.dev.hexagonalarchitectures.application.ports.output;

import com.dev.hexagonalarchitectures.domain.model.Product;

import java.util.Optional;

public interface ProductOutputPort {
    Product saveProduct(Product product);

    Optional<Product> getProductById(Long id);
}
