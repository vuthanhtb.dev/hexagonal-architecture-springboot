package com.dev.hexagonalarchitectures.application.ports.output;

import com.dev.hexagonalarchitectures.domain.event.ProductCreatedEvent;

public interface ProductEventPublisher {
    void publishProductCreatedEvent(ProductCreatedEvent event);
}
