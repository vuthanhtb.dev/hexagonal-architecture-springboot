package com.dev.hexagonalarchitectures.application.ports.input;

import com.dev.hexagonalarchitectures.domain.model.Product;

public interface GetProductUseCase {
    Product getProductById(Long id);
}
